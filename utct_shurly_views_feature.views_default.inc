<?php
/**
 * @file
 * utct_shurly_views_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function utct_shurly_views_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'custom_shurly_my_urls';
  $view->description = 'My short URLs (custom)';
  $view->tag = 'custom';
  $view->base_table = 'shurly';
  $view->human_name = 'Custom shurly_my_urls';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'My URLs';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'View own URL stats';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = 100;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'count' => 'count',
    'destination_1' => 'destination_1',
    'destination' => 'destination',
    'source' => 'source',
    'nothing' => 'nothing',
    'created' => 'created',
    'last_used' => 'last_used',
    'link_delete' => 'link_delete',
  );
  $handler->display->display_options['style_options']['default'] = 'created';
  $handler->display->display_options['style_options']['info'] = array(
    'count' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'destination_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'destination' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'source' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'last_used' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'link_delete' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
  );
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '* Clicks are only tracked once per unique IP address';
  $handler->display->display_options['footer']['area']['format'] = 'plain_text';
  /* Field: Shurly: Clicks */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'shurly';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  /* Field: Shurly: Long URL */
  $handler->display->display_options['fields']['destination_1']['id'] = 'destination_1';
  $handler->display->display_options['fields']['destination_1']['table'] = 'shurly';
  $handler->display->display_options['fields']['destination_1']['field'] = 'destination';
  $handler->display->display_options['fields']['destination_1']['label'] = 'Long URL clipped';
  $handler->display->display_options['fields']['destination_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['destination_1']['alter']['max_length'] = '60';
  $handler->display->display_options['fields']['destination_1']['alter']['trim'] = TRUE;
  /* Field: Shurly: Long URL */
  $handler->display->display_options['fields']['destination']['id'] = 'destination';
  $handler->display->display_options['fields']['destination']['table'] = 'shurly';
  $handler->display->display_options['fields']['destination']['field'] = 'destination';
  $handler->display->display_options['fields']['destination']['exclude'] = TRUE;
  $handler->display->display_options['fields']['destination']['alter']['text'] = '<a href="[destination]" title="[destination]" target="_blank">[destination_1]</a>';
  $handler->display->display_options['fields']['destination']['alter']['path'] = '[destination]';
  $handler->display->display_options['fields']['destination']['alter']['alt'] = '[destination]';
  $handler->display->display_options['fields']['destination']['alter']['target'] = '_blank';
  /* Field: Shurly: Short URL */
  $handler->display->display_options['fields']['source']['id'] = 'source';
  $handler->display->display_options['fields']['source']['table'] = 'shurly';
  $handler->display->display_options['fields']['source']['field'] = 'source';
  $handler->display->display_options['fields']['source']['exclude'] = TRUE;
  $handler->display->display_options['fields']['source']['alter']['path'] = '[source]';
  $handler->display->display_options['fields']['source']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['source']['longshort'] = '0';
  $handler->display->display_options['fields']['source']['link'] = 0;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Links';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="shurly-long">[destination_1]</div>
<div class="shurly-short"><a href="[source]" title="[destination]" target="_blank">[source]</a></div>';
  /* Field: Shurly: Created date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'shurly';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Date';
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'M j';
  /* Field: Shurly: Last used date */
  $handler->display->display_options['fields']['last_used']['id'] = 'last_used';
  $handler->display->display_options['fields']['last_used']['table'] = 'shurly';
  $handler->display->display_options['fields']['last_used']['field'] = 'last_used';
  $handler->display->display_options['fields']['last_used']['label'] = 'Last used';
  $handler->display->display_options['fields']['last_used']['empty'] = 'Never';
  $handler->display->display_options['fields']['last_used']['date_format'] = 'time ago';
  $handler->display->display_options['fields']['last_used']['custom_date_format'] = '1';
  /* Field: Shurly: Delete URL */
  $handler->display->display_options['fields']['link_delete']['id'] = 'link_delete';
  $handler->display->display_options['fields']['link_delete']['table'] = 'shurly';
  $handler->display->display_options['fields']['link_delete']['field'] = 'link_delete';
  $handler->display->display_options['fields']['link_delete']['label'] = '';
  /* Field: Shurly: QR Code */
  $handler->display->display_options['fields']['qr_code']['id'] = 'qr_code';
  $handler->display->display_options['fields']['qr_code']['table'] = 'shurly';
  $handler->display->display_options['fields']['qr_code']['field'] = 'qr_code';
  $handler->display->display_options['fields']['qr_code']['qr_code'] = '0';
  $handler->display->display_options['fields']['qr_code']['fg_color'] = '010b7a';
  /* Sort criterion: Shurly: Created date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'shurly';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Shurly: Current user */
  $handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['table'] = 'shurly';
  $handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['value'] = '1';
  $handler->display->display_options['filters']['uid_current']['group'] = '0';
  $handler->display->display_options['filters']['uid_current']['expose']['operator'] = FALSE;
  /* Filter criterion: Shurly: URL active */
  $handler->display->display_options['filters']['active']['id'] = 'active';
  $handler->display->display_options['filters']['active']['table'] = 'shurly';
  $handler->display->display_options['filters']['active']['field'] = 'active';
  $handler->display->display_options['filters']['active']['value'] = '1';
  $handler->display->display_options['filters']['active']['group'] = '0';
  $handler->display->display_options['filters']['active']['expose']['operator'] = FALSE;

  /* Display: My URLs (custom) */
  $handler = $view->new_display('page', 'My URLs (custom)', 'page_1');
  $handler->display->display_options['path'] = 'myurls';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'My URLs';
  $handler->display->display_options['menu']['description'] = 'List, sort, and delete short URLs';
  $handler->display->display_options['menu']['weight'] = '0';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['block_description'] = 'My short URLs (custom)';
  $export['custom_shurly_my_urls'] = $view;

  return $export;
}
